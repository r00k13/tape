﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixableObject : MonoBehaviour
{
    public GameObject brokenMesh, fixedMesh;

    public void Fix()
    {
        brokenMesh.SetActive(false);
        fixedMesh.SetActive(true);
    }
}
