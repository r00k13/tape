﻿using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;
using UnityEngine.InputSystem;

public class ScoreManager : MonoBehaviour 
{
    public bool tutorialMode = false;

    private int score = 0;
    public GameObject cornerCam;

    public PlayerInput pi;

    public AudioSource tapeSFX;
    public FixedUI fixedUI;

    public TextMeshProUGUI[] scoreTexts;
    public TMP_InputField input;

    public TMP_Text scoreView;

    private AudioSource click;

    public GameObject canvasScore, panelSubmit, panelScores;

    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        click = gameObject.GetComponent<AudioSource>();
		Time.timeScale = 1.0f;
    }


    public IEnumerator EndTutorial()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(2);
    }

    public void IncreaseScore(int scoreAmount)
    {
        tapeSFX.Play();
        score += scoreAmount;
        fixedUI.UpdateScore(score);
        if (tutorialMode && score > 2 ) {
            StartCoroutine(EndTutorial());
            return;
        }
        
    }

    public void EndGame()
    {
        if (tutorialMode)
        {
            SceneManager.LoadScene(1);
        } else
        {
            canvasScore.SetActive(true);
            scoreView.text = "" + score;
            fixedUI.UpdateScore(score);
            cornerCam.SetActive(true);
            pi.enabled = false;
        }
    }


    public void StopTime()
    {
        Time.timeScale = 0;

    }


    public void SubmitScores()
    {
        if (click != null)
        {
            click.Play();
        }
        SetHiScore(score, input.text);
    }

    void SetHiScore(int myScore, string playerName)
    {
        if(playerName == "DELETEALL")
        {
            PlayerPrefs.DeleteAll();
        }
        else
        {
            string scoreKey = "HScore";
            string scoreNameKey = "HScoreName";

            int newScore = myScore;
            string newName = playerName;
            int oldScore;
            string oldName;

            for (int i = 0; i < scoreTexts.Length; i++)
            {
                if (PlayerPrefs.HasKey(scoreKey + i))
                {
                    if (PlayerPrefs.GetInt(scoreKey + i) < newScore)
                    {
                        oldScore = PlayerPrefs.GetInt(scoreKey + i);
                        oldName = PlayerPrefs.GetString(scoreNameKey + i);

                        PlayerPrefs.SetInt(scoreKey + i, newScore);
                        PlayerPrefs.SetString(scoreNameKey + i, newName);
                        newScore = oldScore;
                        newName = oldName;
                    }
                }
                else
                {
                    PlayerPrefs.SetInt(scoreKey + i, newScore);
                    PlayerPrefs.SetString(scoreNameKey + i, newName);
                    newScore = 0;
                    newName = "";
                }
            }
        }
        
        ShowHiScores();
    }

    void ShowHiScores()
    {
        string scoreKey = "HScore";
        string scoreNameKey = "HScoreName";

        for (int i = 0; i < scoreTexts.Length; i++)
        {
            if (PlayerPrefs.GetString(scoreNameKey + i).Length > 0)
            {
                scoreTexts[i].gameObject.SetActive(true);
                scoreTexts[i].text = PlayerPrefs.GetString(scoreNameKey + i) + ": " + PlayerPrefs.GetInt(scoreKey + i);
            }
            else
            {
                break;
            }
        }

        panelSubmit.SetActive(false);
        panelScores.SetActive(true);
    }
}
