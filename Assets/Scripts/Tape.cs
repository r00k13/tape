﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class Tape : MonoBehaviour
{
    private Rigidbody rb;

    public float speed, turnSpeed, jumpForce, stickiness;

    public float tapeLeft = 100f;

    public ScoreManager scoreManager;

    public Transform trail;
    
    private Vector3 normal = Vector3.zero;

    private int touchedObjects = 0;

    public SkinnedMeshRenderer mr;
    
    // Movement Vars
    float h;
    float v;
    bool jump;


    void Start()
    {
        if (scoreManager == null) scoreManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<ScoreManager>();
        rb = GetComponent<Rigidbody>();
    }

    public void OnJump() {
        jump = true;
    }

    public void SetHorizontal(CallbackContext context) {
        h = context.ReadValue<float>();
    }

    public void SetVertical(CallbackContext context) {
        v = context.ReadValue<float>();
    }


    void Update()
    {
        mr.SetBlendShapeWeight(0, 100f - tapeLeft);
    }


    void FixedUpdate()
    {
        float turnAngle = h * turnSpeed;
        
        Vector3 forceDirection = Quaternion.AngleAxis(90, transform.right) * normal;
        forceDirection += transform.right * turnAngle;
        
        rb.AddForce(forceDirection.normalized * speed * v, ForceMode.Force);
        Debug.DrawRay(transform.position, forceDirection.normalized * 10, Color.green);
        
        if (touchedObjects < 1)                                                             //If not stuck to anything
        {
            rb.AddForce(Vector3.up * -15f, ForceMode.Acceleration);
            Debug.DrawRay(transform.position, Vector3.up * -10, Color.red);

            trail.position = transform.position;
        }
        else
        {
            rb.AddForce(normal.normalized * -1 * stickiness, ForceMode.Force);
            Debug.DrawRay(transform.position, normal.normalized * -10, Color.blue);

            trail.position = transform.position - (normal.normalized * 0.04f);
        }

        if (jump)
        {
            jump = false;
            rb.AddForce(normal.normalized * jumpForce, ForceMode.Impulse);
        }

        tapeLeft -= Time.deltaTime * rb.velocity.magnitude;

        if (rb.velocity.magnitude > 0.1f && Time.timeScale > 0)
        {
            float rumblespeed = rb.velocity.magnitude * 0.02f;
            Gamepad.current.SetMotorSpeeds(rumblespeed + (h * -0.04f), rumblespeed + (h * 0.04f));

            if (rb.velocity.magnitude > 0.51f && transform.position.y > -2f)
            {
                rb.AddForce(rb.velocity * -2f, ForceMode.Force);
            }
        }

        if (tapeLeft < 0)
        {
            Gamepad.current.ResetHaptics();
            rb.velocity = Vector3.zero;
            rb.useGravity = false;
            scoreManager.EndGame();
        }

        Debug.Log(rb.velocity.magnitude);
        
    }

    private void OnDestroy()
    {
        Gamepad.current.ResetHaptics();
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject other = collision.gameObject;

        if(other.CompareTag("Broken"))
        {
            other.tag = "Fixed";
            scoreManager.IncreaseScore(1);
            other.GetComponent<FixableObject>().Fix();
        }
        else
        {
            touchedObjects++;
        }
    }

    private void OnCollisionStay(Collision collision)
    {
        normal = Vector3.zero;

        

        foreach(ContactPoint contact in collision.contacts)
        {
            normal += contact.normal;
        }
    }

    public Vector3 GetNormal()
    {
        return normal;
    }

    private void OnCollisionExit(Collision collision)
    {
        if (touchedObjects > 0 && !collision.gameObject.CompareTag("Broken"))
        {
            touchedObjects--;
        }
    }
}
