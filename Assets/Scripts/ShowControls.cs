﻿using UnityEngine;
using UnityEngine.InputSystem;
using static UnityEngine.InputSystem.InputAction;

public class ShowControls : MonoBehaviour
{

    public GameObject[] images;
    public CanvasGroup cg;
    float count = 1f;
    bool fadeOut = false;


    void Start()
    {
        if (Gamepad.current != null) {
            if (Gamepad.current.description.manufacturer.Contains("Sony")) {
                images[1].SetActive(true);
            } else {
                images[2].SetActive(true);
            }
        }
        else
        {
            images[0].SetActive(true);
        }
        cg.alpha = 1;
    }


    public void FadeOut(CallbackContext context)
    {
        if (context.ReadValue<float>() == 0f) return;
        fadeOut = true;
        Debug.Log("FADE OUT");
    }

    void Update()
    {
        if (fadeOut)
        {
            count -= Time.deltaTime;
            cg.alpha = Mathf.Clamp01(count);
            if (count <= 0f) { gameObject.SetActive(false); }
        }
    }
}
