﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TapeTrail : MonoBehaviour
{
    public Transform tape;
    private Tape tapeScript;

    private void Start()
    {
        tapeScript = tape.GetComponent<Tape>();
    }

    // Update is called once per frame
    void Update()
    {
        //transform.position = tape.position;

        if (tapeScript.GetNormal().y != 0)                            //Grounded
        {
            transform.eulerAngles = new Vector3(90, 0, 0);
        }
        else if (tapeScript.GetNormal().z != 0)
        {
            transform.eulerAngles = Vector3.zero;
        }
        else if (tapeScript.GetNormal().x != 0)
        {
            transform.eulerAngles = new Vector3(0, 90, 0);
        }
    }
}
