﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateImage : MonoBehaviour
{
    public RectTransform rt;
    public float speed;

    void Update()
    {
        rt.localEulerAngles = new Vector3(0,0, rt.localEulerAngles.z + (speed * Time.deltaTime));
    }

}
