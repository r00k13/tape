﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedUI : MonoBehaviour
{
    public GameObject[] strips;

    public void UpdateScore(int x)
    {
        for(int i = 0; i < strips.Length; i++)
        {
            strips[i].SetActive((i + 1) <= x);
        }
    }

}
