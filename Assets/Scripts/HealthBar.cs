﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Tape tape;
    public Image healthBar;
    public float minHealth = 0f;
    public float maxHealth = 100f;
    public float health = 100f;
    public Gradient healthBarGradient;

    void Start()
    {
        if (tape == null) tape = GameObject.FindGameObjectWithTag("Player").GetComponentInChildren<Tape>(); ;
    }

    void Update()
    {
        health = tape.tapeLeft;
        healthBar.rectTransform.localScale = Vector3.one * Mathf.Lerp(0.33f, 1.0f, health / maxHealth); ;
        healthBar.color = healthBarGradient.Evaluate(1f - (health / maxHealth));
    }


}
